#ifndef HANGMAN_H
#define HANGMAN_H
#include <iostream> //Defines standard input/output stream objects
#include <string>   //Introduces string types, character traits and some converting functions
#include <algorithm>//Defines collection of functions designed to be used on ranges of elements
#include <cctype>   //Used to transform individual characters - in this case the users input to all uppercase
#include <ctime>    //Defines functions to get and manipulate date and time info
using namespace std; //Used so that std:: does not have to be prefixxed constantly

class Hangman {
	public:
		Hangman(int option = 1);	//Constructor
		void GameLoop();	//Function to actually play game

	private:
		string THE_WORD;
		int MAX_WRONG;
		int LIVES;
		string guessed;
};
#endif				
