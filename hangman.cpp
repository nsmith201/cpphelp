#include "hangman.h"
#include <iostream> //Defines standard input/output stream objects
#include <string>   //Introduces string types, character traits and some converting functions
#include <algorithm>//Defines collection of functions designed to be used on ranges of elements
#include <cctype>   //Used to transform individual characters - in this case the users input to all uppercase
#include <ctime>    //Defines functions to get and manipulate date and time info
using namespace std; //Used so that std:: does not have to be prefixxed constantly

Hangman::Hangman(int option){
	MAX_WRONG = 6;                  //Initialises a constant MAX_WRONG and sets it to 6
	LIVES = MAX_WRONG;
	srand(time(0));                           //Line needed so that the random number generator further down does not constantly generate the same number.
	int randomNumber = rand()%20 ;                //This sets up a variable iSecret as a random number between 0 and 7
	
	const string countries[20] = {"ITALY","FRANCE","IRELAND","ENGLAND","WALES","SCOTLAND","GERMANY","SWEDEN","JAPAN","CHINA","THAILAND","BELGIUM","BRAZIL","EGYPT","ICELAND","MEXICO","NORWAY","SWITZERLAND","CANADA","RUSSIA"};

	const string animals[20] = {"ZEBRA", "GIRAFFE", "HORSE", "TURTLE", "SNAKE", "ELEPHANT", "RHINO", "EAGLE", "TIGER", "MONKEY", "PANDA", "OSTRICH", "PENGUIN", "MOUSE", "OCTOPUS", "CROCODILE", "RABBIT", "LIZARD", "CHICKEN", "CAMEL"};

	if(option == 1)
		THE_WORD = countries[randomNumber];
	else
		THE_WORD = animals[randomNumber];

	guessed = "";
}

void Hangman::GameLoop(){
	//Intro
	cout <<"Welcome to Hangman. \n"; //Prints this message at the top of the window
	cout <<"To guess a letter, simply type it and press enter.\n\n";

	//Setups
	char LETTER;                              //Sets up "LETTER" as a character

	cout << this->THE_WORD << endl;
	cout << this->LIVES;
	// Fetching Word from Array and Setting up series of dashes
	string soFar(this->THE_WORD.size(), '-');            // This sets up a string of dashes equal to the number of letters in the country name

	//Starting the main while loop
	while ((this->LIVES != 0) && (soFar != this->THE_WORD)) //While the user has lives left, and has not guessed the word:
	
	{
	//User inputs guess
	cout << "\n" << (soFar) << "\n";              //Shows the series of dashes (soFar) to begin with
	cout <<"Enter your guess \n";                 // Message to ask user to input a letter
	cin >> LETTER;                                // User inputs a letter as a guess
	LETTER = toupper(LETTER);                     // This function is defined the cctype file. Changes the user input to uppercase (to match the set list of words).
	this->guessed += LETTER;                            // Adds LETTER to the "guessed" string

	if (this->THE_WORD.find(LETTER) !=string::npos){     //If LETTER is in the word, it does the following:
      		for (int i = 0; i < this->THE_WORD.length(); ++i){
        		if(this->THE_WORD[i] == LETTER)
        			soFar[i] = LETTER;
      		}
  	}
	else {--LIVES;}                                //If the letter is not in the word, a life is taken away from the user.


	//Guesses Left + Gallows ASCII Drawings
	cout <<"You have " << (LIVES) <<" incorrect guesses left\n";                                     //Line to tell the user how many lives they have left
	cout <<"Letters Guessed: " << (guessed) << "\n";                                                 //Shows the user the string of letters they have used already
	if(LIVES==6){cout << "   ______\n   |     |\n   |\n   |\n   |\n   |_______";}                    // ASCII Art for Empty Gallows to print if 6 lives left
	if(LIVES==5){cout << "   ______\n   |     |\n   |     0\n   |\n   |\n   |_______";}              //Art for 5 Lives left
	if(LIVES==4){cout << "   ______\n   |     |\n   |     0\n   |     |\n   |\n   |_______";}        //Art for 4 Lives Left
	if(LIVES==3){cout << "   ______\n   |     |\n   |     0\n   |    /|\n   |\n   |_______";}        //Art for 3 Lives Left
	if(LIVES==2){cout << "   ______\n   |     |\n   |     0\n   |    /|\\ \n   |\n   |_______";}     //Art for 2 Lives Left - Note the use of 2 '\' as first one is ignored.
	if(LIVES==1){cout << "   ______\n   |     |\n   |     0\n   |    /|\\ \n   |    /\n   |_______ \n Careful! One Guess Left!";} // Art for 1 Life Left
	if(LIVES==0){cout << "   ______\n   |     |\n   |     0\n   |    /|\\ \n   |    /\\ \n   |_______ \n GAME OVER\n";} //Game Over Art
	if(soFar == THE_WORD){cout << "\nYou Win! Congratulations. \n The word was " << (THE_WORD);}// Winning Message
	}
}
