#include "hangman.h"
#include <iostream>

using namespace std;

int main(){
	int OPTION;
	cout << "Please Select a category by typing 1 or 2.\n 1.Countries (1)\n 2.Animals (2).\n";
	cin >> OPTION;
	Hangman newgame(OPTION);
	newgame.GameLoop();
	return 0;
}	
