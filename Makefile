all: hangman

hangman.o: hangman.cpp hangman.h
	g++ -c hangman.cpp
hangman: main.cpp hangman.o
	g++ main.cpp hangman.o -o hangman
clean:
	rm hangman *.o
